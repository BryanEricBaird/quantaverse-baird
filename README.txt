#####
##### Quantaverse Software Engineering Submission
##### by Bryan Eric Baird, bryan.eric.baird@mg.thedataincubator.com
##### February 12, 2018
#####

--Requirements:--

Python version 2.7.X

Packages:
- Pandas
- Multiprocessing
- NumPy
- Datetime


--Run Instructions:--

Run the script "laundering.py" from the correct directory.
Note that it will look for the underlying data in the "data" folder in the same directory.


--Assumptions:--

- For data encoded to the (non-existant) date of February 29, 2006, they are "corrected" to be the 28th instead.
- Suspicious entities are double-counted if they are the "middleman" in the transaction, because they both send and receive.