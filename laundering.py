#!/usr/bin/python2.7.X

print 'Initializing....'
import pandas as pd
import multiprocessing as mp
import datetime
import numpy as np

def getSuspicious(date, buffer_days = 0, threshold = 0.1):
    """
    Finds transaction ids for suspicious transactions on or around a given date.
    
    Arguments:
        date: The central date to be checked, in datetime format.
        buffer_days: How many days on either side of the target date to include, default 0.
        threshold: Level of "off the top" that constitutes a "suspicious" bridging transaction.
            Default 0.1, or 10%.
        
    Output:
        A tuple of length two, with each element consisting of a list of ids for suspicious transactions
        on the date/dates in question. The first element 
    """
    # First, check the number of adjacent days as determined by buffer_days
    # Default value of 0 will only check transactions on the exact same day    

    date_range = (date - np.timedelta64(buffer_days, 'D'), date + np.timedelta64(buffer_days, 'D'))
    
    local = df[(df['TIMESTAMP'] >= date_range[0]) & (df['TIMESTAMP'] <= date_range[1])]
    
    # Merge the transaction data from the daterange on itself, checking for senders who are also receivers
    # within the date range. The resulting rows are all 2-step "chained" transactions, where one person
    # receives a payment and then also sends a payment within the time range.
   
    output = pd.merge(local, local, how='inner', left_on='RECEIVER', right_on='SENDER',
         left_index=False, right_index=False, sort=True,
         suffixes=('_first', '_second'))
    
    
    # Calculates the ratio that is withheld in the chained transaction, i.e. the difference in the two payment
    # amounts, divided by the first amount. If this value is between 0 and our specificied threshold (default
    # 10%, or 0.1), then it is a good indicator of the "off the top" cut that the bridge is taking for their
    # services.
    
    output['DIFF_RATIO'] = (output['AMOUNT_first'] - output['AMOUNT_second']) / output['AMOUNT_first']

    output = output[(output['DIFF_RATIO'] < threshold) & (output['DIFF_RATIO'] > 0)]
    
    # Return suspicious transaction ids, whether they are the first or second in the chain.
    
    return([list(output['TRANSACTION_first']), list(output['TRANSACTION_second'])])



###
### Part 1: Loading and Cleaning Data
###

print 'Loading Data....',
df = pd.read_csv('data/transactions.zip', sep = '|')

# The data contains approx. 17,000 transactions that occur on '2006-02-29'.
# This date does not exist (2006 was not a leap year), so the transactions are reassigned to 2006-02-28 instead
# 
# It is not clear from the raw data inspection what the miscoding here would be exactly (e.g., could be 2016-02-29),
# but 2006-02-28 is not a 'crowded' date yet and mostly preserves the relationships.

df['TIMESTAMP'].replace(['2006-02-29'], '2006-02-28', inplace=True)
df['TIMESTAMP'] = pd.to_datetime(df['TIMESTAMP'], format="%Y-%m-%d")

# There are also a number of "self-transactions" reported, where the sender and recipient are the same.
# These observations are dropped, as it cannot be inferred who the participant parties would be.

df = df[df['SENDER'] != df['RECEIVER']]
print 'Done!'


###
### Part 2: Finding Suspicious Transactions
###

print 'Finding Suspicious Transactions....',
dates = df['TIMESTAMP'].unique()

suspect_transactions = []

# Generates a multiprocess pool to check multiple dates at a time in parallel, appending results to list.
pool = mp.Pool(processes = (mp.cpu_count() - 1))   
suspect_transactions.extend(pool.map(getSuspicious, dates))

# Commented code below is a non-parallelized equivalent for checking each date in series.
# for date in dates:
#     suspect_transactions.extend(getSuspicious(date))

# With pooling implementation, result is list of list of lists. This comprehesion flattens to one level.
suspect_transactions = [item for sublist in suspect_transactions for subsublist in sublist for item in subsublist]

# List is transaction ids, possibly with duplicates. Select matching rows from original data for extra context, while
# removing duplicates for free.
transaction_output = df[df['TRANSACTION'].isin(suspect_transactions)]

# With the list having served its purpose, the pool can be safely closed.
pool.close()
print 'Done!'

# Save the matched DataFrame to a CSV, with one row per suspicious transaction.
print 'Saving Suspicious Transactions CSV....',
transaction_output.to_csv('suspicious_transactions.csv', index = False)
print 'Done!' 


###
### Part 3: Ranking Suspicious Entities
###

# Counts the number of times each entity appears as either the sender OR recipient on a suspicious transaction.
# IMPORTANT NOTE: This means that the "bridges" or "middlemen" will be DOUBLE-COUNTED, both as the recipient
# of the first payment, and the sender of the second. 

print 'Generating And Saving Suspicious Entities CSV....',

suspect_receivers = transaction_output.groupby('RECEIVER', as_index = False).count()
suspect_receivers = suspect_receivers[['RECEIVER', 'TRANSACTION']]
suspect_receivers.columns = ['ENTITY', 'TRANSACTION_COUNT']

suspect_senders = transaction_output.groupby('SENDER', as_index = False).count()
suspect_senders = suspect_senders[['SENDER', 'TRANSACTION']]
suspect_senders.columns = ['ENTITY', 'TRANSACTION_COUNT']

# Combines the counts of appearances of each entity as a sender and receiver, and saves the ranked result to CSV.

entities_output = suspect_receivers.append(suspect_senders, ignore_index = True)
entities_output = entities_output.groupby('ENTITY').sum()
entities_output.sort_values('TRANSACTION_COUNT', inplace = True, ascending = False)

entities_output.to_csv('suspicious_entities.csv')
print 'Done!'